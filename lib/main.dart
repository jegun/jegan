import 'package:cart_app/ui/login_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'core/helpers/app_colors.dart';
import 'core/helpers/app_fonts.dart';
import 'core/providers/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (context) => StateProvider(),
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: AppFont.fontFamilyName,
        textTheme: TextTheme(
          button: TextStyle(fontWeight: AppFont.fontWeightRegular),
          caption: TextStyle(fontWeight: AppFont.fontWeightRegular),
          body1: TextStyle(fontWeight: AppFont.fontWeightRegular),
          body2: TextStyle(fontWeight: AppFont.fontWeightRegular),
          subhead: TextStyle(fontWeight: AppFont.fontWeightRegular),
          title: TextStyle(fontWeight: AppFont.fontWeightRegular),
          headline: TextStyle(fontWeight: AppFont.fontWeightRegular),
          display1: TextStyle(fontWeight: AppFont.fontWeightRegular),
          display2: TextStyle(fontWeight: AppFont.fontWeightRegular),
        ),
        primaryColor: colorPrimary,
        accentColor: colorAccent,
        buttonColor: colorPrimary,
        buttonTheme: const ButtonThemeData(
            buttonColor: colorPrimary, textTheme: ButtonTextTheme.primary),
      ),
      debugShowCheckedModeBanner: false,
      home: LoginScreen(),
    );
  }
}


