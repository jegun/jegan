// import 'dart:math';
//
// import 'package:cart_app/core/helpers/app_colors.dart';
// import 'package:cart_app/core/helpers/app_constants.dart';
// import 'package:cart_app/core/helpers/app_fonts.dart';
// import 'package:cart_app/core/helpers/app_textstyle.dart';
// import 'package:flutter/material.dart';
//
//
// class WidgetProductItem extends StatelessWidget {
//
//
//   WidgetProductItem();
//
//   @override
//   Widget build(BuildContext context) {
//     return
//       Card(
//         elevation: 1.0,
//         color: colorGrey,
//         child: Container(
//           child: Column(
//             mainAxisSize: MainAxisSize.max,
//             children: <Widget>[
//               InkWell(
//                 splashColor: colorPrimary,
//                 child: Card(
//                   elevation: 0.0,
//                   color: colorWhite,
//                   child: Padding(
//                     padding: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
//                     child: AspectRatio(
//                       aspectRatio: 1 / 0.95,
//                       child: Container(
//                         width: (getScreenWidth(context) / 2) - 32,
//                         child: Stack(
//                           children: <Widget>[
//                             Center(
//                               child: Padding(
//                                 padding: const EdgeInsets.all(16.0),
// //                              child: Image.asset(itemProduct.imageUrl),
//                                 child: (itemProduct.imageUrl != null &&
//                                     itemProduct.imageUrl != "")
//                                     ? CachedNetworkImage(
//                                     imageUrl: itemProduct.imageUrl)
//                                     : Image.asset(
//                                     "assets/logo_thought_factory.png"),
//                               ),
//                             ),
//                             Align(
//                               alignment: Alignment.topRight,
//                               child: Container(
//                                 width: 30,
//                                 height: 30,
//                                 child: FloatingActionButton(
//                                   heroTag: "${Random().nextDouble()}",
//                                   elevation: 2.0,
//                                   onPressed: () =>
//                                       {},
//                                   mini: true,
//                                   backgroundColor: colorWhite,
//                                   child: Consumer<ItemProduct>(
//                                     builder: (context, value, _) =>
//                                     value.isFavourite
//                                         ? _buildIconHeart(colorPrimary)
//                                         : _buildIconHeart(colorDarkGrey),
//                                   ),
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//               Padding(
//                 padding: const EdgeInsets.only(
//                     top: 4.0, left: 4.0, right: 4.0, bottom: 0.0),
//                 child: Align(
//                   child:
// //                  Text(
// //                    itemProduct.name,
// //                    style: getProductNameTextStyle(context),
// //                    maxLines: 2,
// //                  ),
//                   AutoSizeText(
//                     itemProduct.name,
//                     maxLines: 2,
//                     style: getProductNameTextStyle(context),
//                     overflow: TextOverflow.ellipsis,
//                   ),
//                   alignment: Alignment.centerLeft,
//                 ),
//               ), //text: product name
//               Padding(
//                 padding:
//                 const EdgeInsets.only(left: 4.0, right: 4.0, bottom: 2.0),
//                 child: Align(
//                   child: (itemProduct.specialPrice == null ||
//                       itemProduct.specialPrice == 0.0)
//                       ? Text(
//                     "AED ${double.parse(itemProduct.price.toString()).toStringAsFixed(2) ?? ""} ",
//                     style: getProductAmtTextStyle(context),
//                   )
//                       : Row(
//                     children: <Widget>[
//                       Text(
//                         "AED ${double.parse(itemProduct.specialPrice.toString()).toStringAsFixed(2) ?? ""} ",
//                         style: getProductAmtTextStyle(context),
//                       ),
//                       Text(
//                         '${double.parse(itemProduct.price.toString()).toStringAsFixed(2) ?? ""}',
//                         style: getStyleCaption(context).copyWith(
//                             decoration: TextDecoration.lineThrough),
//                       )
//                     ],
//                   ),
//                   alignment: Alignment.centerLeft,
//                 ),
//               ), //text: product price in dollar
//               Container(
//                   alignment: Alignment.center,
//                   padding:
//                   const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 4.0),
//                   child: isCompareModeOn
//                       ? _buildCompareModeOption(
//                       itemProduct, context, onPressedItemCompareButton)
//                       : _buildAddRemoveQuantityOption(
//                       itemProduct, onPressedRemove, onPressedAdd))
//             ],
//           ),
//         ),
//       );
//     ;
//   }
// }
//
// Widget _buildCompareModeOption(context) {
//   return
//     Container(
//       width: double.infinity,
//       child:
//       RaisedButton(
//         color: !value.isAddedToCompare ? colorPrimary : colorWhite,
//         elevation: 1,
//         padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
//         child: Text(
//           !value.isAddedToCompare ? AppConstants.ADDED : AppConstants.COMPARE,
//           style: getStyleBody1(context).copyWith(
//               color: !value.isAddedToCompare ? colorWhite : colorPrimary,
//               fontWeight: AppFont.fontWeightSemiBold),
//         ),
//         onPressed: () => onPressedItemCompareButton(
//             value) /*() {
//           value.isAddedToCompare = (!itemProduct.isAddedToCompare);
//         }*/
//         ,
//         shape:
//         RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
//       ),
//     );
// }
//
//
