import 'package:shared_preferences/shared_preferences.dart';

import 'app_constants.dart';


class AppSharedPreference {

  // singleton boilerplate
  AppSharedPreference._internal();

  static final AppSharedPreference _singleInstance = AppSharedPreference._internal();

  factory AppSharedPreference() => _singleInstance;





  Future<bool> save(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(key, value);
  }



  //save: customer id
  Future<bool> saveCustomerId(String customerId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(AppConstants.KEY_CUSTOMER_ID, customerId);
  }



  Future<bool> setPreferenceData(String key,String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(key,value);
  }


  Future saveStringValue(String keyName, String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString(keyName, value);
  }

  // Future<String> getStringValue(String keyName) async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   return prefs.getString(keyName);
  // }

  Future saveBooleanValue(String keyName, bool value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool(keyName, value);
  }


}
