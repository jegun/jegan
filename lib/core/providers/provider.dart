import 'package:cart_app/core/Models/product_models.dart';
import 'package:flutter/material.dart';

class StateProvider with ChangeNotifier{
  List<Products> Mobileitems = List<Products>.empty(growable: true);
  List<Products> Laptopsitems = List<Products>.empty(growable: true);

  List<Products> Tvsitems = List<Products>.empty(growable: true);

  List<Products> Watchesitems = List<Products>.empty(growable: true);

  List<Products> Headphonesitems = List<Products>.empty(growable: true);


  // Operations
  // void editTask(Todo item, String description){
  //   if(description != null && description != ''){
  //     item.description = description;
  //     notifyListeners();
  //   }
  // }

  void removeMobileItem(Products item){
    Mobileitems.remove(item);
    print("deletemobile");
    notifyListeners();
  }
  void removeLapItem(Products item){
    Laptopsitems.remove(item);
    print("dlt list");
    notifyListeners();
  }
  void removeTVItem(Products item){
    Tvsitems.remove(item);
    notifyListeners();
  }
  void removeWatchesItem(Products item){
    Watchesitems.remove(item);
    notifyListeners();
  }
  void removeHeadphonesItem(Products item){
    Headphonesitems.remove(item);
    notifyListeners();
  }
  // void removeItem(Products item){
  //   if(tabindex == 0){
  //     Mobileitems.remove(item);
  //     print(Mobileitems);
  //   }
  //   else if(tabindex == 1){
  //     Laptopsitems.remove(item);
  //     print(Laptopsitems);
  //
  //
  //   }
  //   else if(tabindex == 2){
  //     Tvsitems.remove(item);
  //     print(Tvsitems);
  //
  //
  //   }
  //
  //   else if(tabindex == 3){
  //     Watchesitems.remove(item);
  //
  //   }
  //   else if(tabindex == 4){
  //     Headphonesitems.remove(item);
  //
  //   };
  //   notifyListeners();
  // }

  void addNewTask(String productname, String modelnumber,String price,String Description,int tabindex) {

    if(productname != null && productname != ''&& modelnumber != null && modelnumber != '' && price != null && price != ''&& Description != null && Description != ''){
      if(tabindex == 0){
        Mobileitems.add(Products(productname,modelnumber,price,Description));
        print(Mobileitems);
      }
      else if(tabindex == 1){
        Laptopsitems.add(Products(productname,modelnumber,price,Description));
        print(Laptopsitems);


      }
      else if(tabindex == 2){
        Tvsitems.add(Products(productname,modelnumber,price,Description));
        print(Tvsitems);


      }

      else if(tabindex == 3){
        Watchesitems.add(Products(productname,modelnumber,price,Description));

      }
    else if(tabindex == 4){
        Headphonesitems.add(Products(productname,modelnumber,price,Description));

    };
      notifyListeners();

      // sharedPrefs.setString("saved", json.encode(Books(description,authername)));
      // json.decode(sharedPrefs.getString("saved"));
      // print(json.decode(sharedPrefs.getString("saved")));
    }
  }

  void chanceCompleteness(Products item){
    item.complete = !item.complete;
    notifyListeners();
  }
}