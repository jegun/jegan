import 'package:cart_app/core/helpers/app_colors.dart';
import 'package:cart_app/core/helpers/app_constants.dart';
import 'package:cart_app/core/helpers/app_fonts.dart';
import 'package:cart_app/core/helpers/app_sharedPref.dart';
import 'package:cart_app/core/helpers/app_textstyle.dart';
import 'package:cart_app/core/helpers/app_validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';


class RegisterScreen extends StatefulWidget {


  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  static var _keyValidationForm = GlobalKey<FormState>();
  TextEditingController _textEditConFirstName = TextEditingController();
  TextEditingController _textEditConLastName = TextEditingController();
  TextEditingController _textEditConEmail = TextEditingController();
  TextEditingController _textEditConPassword = TextEditingController();

  final FocusNode _fNodeLastName = FocusNode();
  final FocusNode _fNodeEmail = FocusNode();
  final FocusNode _fNodeTRN = FocusNode();


  bool isPasswordVisible = false;
  bool isConfirmPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    isPasswordVisible = false;
    isConfirmPasswordVisible = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
        key: _scaffoldKey,
        backgroundColor: colorGrey,
        body:
        SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.only(bottom: 32.0),
              child: Column(
                children: <Widget>[
                  _buildWidgetImageLogo(),
                  getWidgetRegistrationCard(),
                ],
              )),
        ),
      );
  }

  Widget _buildWidgetImageLogo() {
    return Container(
      padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: Image.network(
    "https://www.freepnglogos.com/uploads/shopping-cart-png/shopping-cart-campus-recreation-university-nebraska-lincoln-30.png",

    width: 90,
        height: 120,
      ),
    );
  }

  Widget getWidgetRegistrationCard() {
    return
      Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: Container(
        margin: EdgeInsets.only(bottom: 32.0),
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppConstants.FORM_CARD_CORNER),
          ),
          elevation: 10.0,
          child: Padding(
            padding: const EdgeInsets.only(
                top: 24.0, bottom: 16.0, left: 24.0, right: 24.0),
            child: Form(
              key: _keyValidationForm,
              child: Column(
                children: <Widget>[
                  Text(
                    'Register',
                    style: getFormTitleStyle(context),
                  ), // title: Register
                  SizedBox(
                    height: 12.0,
                  ),
                  TextFormField(
                    controller: _textEditConFirstName,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    textCapitalization: TextCapitalization.sentences,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter Username';
                      }
                      return null;
                    },
                    style: getFormStyleText(context),
                    onFieldSubmitted: (String value) {
                      FocusScope.of(context).requestFocus(_fNodeLastName);
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      labelText: 'Username name',
                      labelStyle: getFormLabelStyleText(context),
                      contentPadding: EdgeInsets.only(left: 0),

                    ),
                  ), //text field : user name
                  SizedBox(
                    height: 12.0,
                  ),
                  TextFormField(
                    controller: _textEditConLastName,
                    focusNode: _fNodeLastName,
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    textCapitalization: TextCapitalization.sentences,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter Password';
                        }
                        return null;
                      },
                    style: getFormStyleText(context),
                    onFieldSubmitted: (String value) {
                      FocusScope.of(context).requestFocus(_fNodeEmail);
                    },
                    decoration: InputDecoration(
                      isDense: true,
                      labelText: 'Password',
                      labelStyle: getFormLabelStyleText(context),
                      contentPadding: EdgeInsets.only(left: 0),

                    ),
                  ),
                  SizedBox(
                    height: 12.0,
                  ),
                  TextFormField(
                    controller: _textEditConEmail,
                    focusNode: _fNodeEmail,
                    style: getFormStyleText(context),
                    keyboardType: TextInputType.emailAddress,
                    textCapitalization: TextCapitalization.sentences,
                    textInputAction: TextInputAction.next,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter Date of Birth';
                      }
                      return null;
                    },
                    onFieldSubmitted: (String value) {
                      FocusScope.of(context).requestFocus(_fNodeTRN);
                    },
                    decoration: InputDecoration(
                        isDense: true,
                        labelText: 'Date of Birth',
                        labelStyle: getFormLabelStyleText(context),
                        contentPadding: EdgeInsets.only(left: 0),
                        ),
                  ), //text field: email

                  Container(
                    margin: EdgeInsets.only(top: 32.0),
                    width: double.infinity,
                    child: RaisedButton(
                      color: colorAccent,
                      textColor: Colors.white,
                      elevation: 5.0,
                      padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Text(
                        'Register',
                        style: getStyleButtonText(context),
                      ),
                      onPressed: () {
                        if (_keyValidationForm.currentState!.validate()) {
                          print("validated");
                          _onClickButtonRegister(context,_textEditConFirstName.text,_textEditConLastName.text,_textEditConEmail.text);
                        }else{
                          print("no");
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                  ), //button: login
                  Container(
                      margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Already Registered?  ',
                            style: getFormNormalTextStyle(context),
                          ),
                          InkWell(
                            splashColor: colorAccent.withOpacity(0.5),
                            onTap: () {
                              _onTappedTextLogin();
                            },
                            child: Text(
                              '  Login',
                              style: getSmallTextNavigationStyle(context),
                            ),
                          )
                        ],
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }




  // String _validatePassword(String value) {
  //   if (value.length == 0) {
  //     return 'Password is required';
  //   } else if (value.length < 8) {
  //     return 'Min 8 char required';
  //   }
  // }


  void _onClickButtonRegister(
      BuildContext context,String Username, String Password,String DOB) async {
    print("hello");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('username', Username);
    prefs.setString('password', Password);
    prefs.setString('dob', DOB);

    _showDialogueSuccessRegistration(context);
  }

  void _onTappedTextLogin() {
    Navigator.pop(context);
  }

  void _showDialogueSuccessRegistration(context) {
    print("ddd");
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              'Successfully Registered !',
              style: getAppBarTitleTextStyle(context).copyWith(
                  color: colorBlack, fontWeight: AppFont.fontWeightSemiBold),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(AppConstants.OK,
                    style: getStyleSubHeading(context)!.copyWith(
                        color: colorPrimary,
                        fontWeight: AppFont.fontWeightSemiBold)),
                onPressed: () {
                  _saveUserCred();
                  if (Navigator.canPop(context)) {
                    Navigator.pop(context); //for dialogue
                  }
                  if (Navigator.canPop(context)) {
                    Navigator.pop(context); //for register screen
                  }
                },
              )
            ],
          );
        });
  }

  void _saveUserCred() async {
    AppSharedPreference().saveStringValue(
        AppConstants.KEY_USER_EMAIL_ID, _textEditConEmail.text);
    AppSharedPreference().saveStringValue(
        AppConstants.KEY_USER_PASSWORD, _textEditConPassword.text);
  }
}
