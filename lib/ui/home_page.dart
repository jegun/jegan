
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:cart_app/core/Models/product_models.dart';
import 'package:cart_app/core/helpers/app_colors.dart';
import 'package:cart_app/core/helpers/app_textstyle.dart';
import 'package:cart_app/core/providers/provider.dart';
import 'package:cart_app/ui/product_detail_page.dart';
import 'package:cart_app/widgets/product_item_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'add_item.dart';
import 'list_view.dart';


class MyHomePage extends StatefulWidget {
  final String username;
  final String imageurl;
  MyHomePage(this.username,this.imageurl);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with SingleTickerProviderStateMixin {
  final List<Widget> myTabs = [
    Tab(text: 'Mobiles'),
    Tab(text: 'Laptops'),
    Tab(text: 'Tvs'),
    Tab(text: 'watches'),
    Tab(text: 'Headphones'),

  ];


  late TabController _tabController;
  Future<SharedPreferences> _sharePrefs = SharedPreferences.getInstance();
  late DateTime currentBackPressTime;
var username;
var _activeTabIndex;

  // save(String key, Books) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString(key, json.encode(items));
  //   final r =  json.decode(prefs.getString(key));
  //   print(r);
  //
  // }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _tabController = TabController(length: 5, vsync: this);
    _tabController.addListener(_handleTabSelection);
    _setActiveTabIndex();
    super.initState();
  }


  void _setActiveTabIndex() {
    _activeTabIndex = _tabController.index;
    print(_activeTabIndex);
  }
  _handleTabSelection() {
    if (_tabController.indexIsChanging) {
      setState(() {

        _activeTabIndex = _tabController.index;


      });
    }
  }
  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to Logout your App'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          TextButton(
            onPressed: () => {
              Navigator.of(context).pop(true),
              Navigator.of(context).pop()

            },
            child: new Text('Yes'),
          ),
        ],
      ),
    )) ?? false;
  }

  _listItem() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Colors.blueAccent,
        ),
      ),
      height: 120,
      child: Center(
        child: Text('List Item', style: TextStyle(fontSize: 20.0)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(child: Scaffold(
        appBar: AppBar(
          title: Row(children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: Container(
                height: 40.0,
                width: 30.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(
                      5.0) //                 <--- border radius here
                  ),
                  image: DecorationImage(
                    image: NetworkImage(widget.imageurl),
                    fit: BoxFit.fill,
                  ),
                  shape: BoxShape.rectangle,
                ),
              ),
            ),
            SizedBox(width: 10,),
            Text("Hi " + widget.username),
            Spacer(),
            InkWell(
              child: Text("Logout",style:TextStyle(color: Colors.white,fontSize: 10)),
              onTap: (){
                _onWillPop();
              },
            )
          ],),
          automaticallyImplyLeading: false,
          toolbarHeight: 100,
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            goToNewItemView();
            // Add your onPressed code here!
          },
          label: const Text('Add Item'),
          icon: const Icon(Icons.add),
          backgroundColor: colorPrimary,
        ),
        body:

        Consumer<StateProvider>(
            builder: (context, stateProvider, child) {
              return

                ListView(
                  children: <Widget>[
                    TabBar(
                      isScrollable: true,
                      controller: _tabController,
                      indicator: BoxDecoration(
                        color: colorPrimary,
                      ),
                      labelColor: Colors.white,
                      unselectedLabelColor: Colors.black,
                      tabs: myTabs,
                    ),

                    Center(
                      child: [
                        Column(
                          children: [
                            _buildSliverGridProducts(stateProvider.Mobileitems.length,stateProvider.Mobileitems,stateProvider.removeMobileItem,stateProvider.Mobileitems.isNotEmpty)
                          ],
                        ),
                        Column(
                          children: [
                            _buildSliverGridProducts(stateProvider.Laptopsitems.length,stateProvider.Laptopsitems,stateProvider.removeLapItem,stateProvider.Laptopsitems.isNotEmpty)
                          ],
                        ),
                        Column(
                          children: [
                            _buildSliverGridProducts(stateProvider.Tvsitems.length,stateProvider.Tvsitems,stateProvider.removeTVItem,stateProvider.Tvsitems.isNotEmpty)
                          ],
                        ),
                        Column(
                          children: [
                            _buildSliverGridProducts(stateProvider.Watchesitems.length,stateProvider.Watchesitems,stateProvider.removeWatchesItem,stateProvider.Watchesitems.isNotEmpty)
                          ],
                        ),
                        Column(
                          children: [
                            _buildSliverGridProducts(stateProvider.Headphonesitems.length,stateProvider.Headphonesitems,stateProvider.removeHeadphonesItem,stateProvider.Headphonesitems.isNotEmpty)
                          ],
                        ),
                      ][_tabController.index],
                    ),

                  ],
                )
              ;
            }
        )

    ), onWillPop: _onWillPop);


  }

  Widget _buildSliverGridProducts(int count,List<Products> product,Function(Products) onDismissed,bool checkempty) {
    return
      Consumer<StateProvider>(
        builder: (context, stateProvider, child) {
          return checkempty
              ?Column(
            children: [
              Container(
                height: MediaQuery.of(context).size.height/10,
                child:  Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(6.0))),
                  child:  Container(
                    child:
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("Product Name"),
                        Text("Model number"),
                        Text("Price"),

                      ],
                    ),
                  ),
                ),
              ),

              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: count,
                itemBuilder: (context, index) {
                  return ProductItem(
                    item: product[index],
                    onDismissed:onDismissed,
                  );
                },
              )
            ],
          )

              : Center(child: Text('No items'));
        }
    );

  }
  void goToNewItemView(){
    print("huh");


    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) {
          return ItemView(_activeTabIndex);
        }
    )
    );
  }

  // void goToEditItemView(Products item){
  //   Navigator.of(context).push(MaterialPageRoute(
  //       builder: (context) {
  //         return ItemView();
  //       }
  //   ));
  // }




}



class ProductItem extends StatelessWidget {
  final Products item;
  final Function(Products) onDismissed;

  ProductItem({
    required this.item,
    required this.onDismissed,
  });

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key(item.hashCode.toString()),
      direction: DismissDirection.startToEnd,
      background: Container(
        padding: EdgeInsets.only(left: 12),
        color: Colors.red,
        child: Icon(Icons.delete, color: Colors.white,),
        alignment: Alignment.centerLeft,
      ),
      onDismissed: (direction) => onDismissed(item),
      child: ListTile(
        title:Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(item.productname,
              style: TextStyle(decoration: item.complete
                  ? TextDecoration.lineThrough
                  : null),
            ),
            Text(item.modelnumber,
              style: TextStyle(decoration: item.complete
                  ? TextDecoration.lineThrough
                  : null),
            ),
            Text(item.price,
              style: TextStyle(decoration: item.complete
                  ? TextDecoration.lineThrough
                  : null),
            ),

          ],
        ),
        onTap: () => {
        Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => ProductDetailScreen(item.productname,item.modelnumber,item.price,item.Description)),
        )

        },
      ),
    );
  }
}





