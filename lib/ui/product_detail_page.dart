import 'package:cart_app/core/helpers/app_colors.dart';
import 'package:cart_app/core/helpers/app_fonts.dart';
import 'package:cart_app/core/helpers/app_textstyle.dart';
import 'package:flutter/material.dart';



class ProductDetailScreen extends StatefulWidget {
  final String productname;
  final String modelnumber;
  final String price;
  final String description;

  ProductDetailScreen(this.productname,this.modelnumber,this.price,this.description);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Product Details"),
        centerTitle: true,
      ),
          backgroundColor: colorGrey,
          body: _buildProductDetailScreen(context)

      );
  }


   _buildProductDetailScreen(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[

            Card(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(6.0))),
              child: Container(
                padding: EdgeInsets.all(4),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      width: 16.0,
                    ),
                    Flexible(
                        fit: FlexFit.loose,
                        child: Center(
                          child:  Container(
                            height: MediaQuery.of(context).size.height/2,
                            width: MediaQuery.of(context).size.width/2,

                            child: Image.network('https://www.ncenet.com/wp-content/uploads/2020/04/no-image-png-2.png'),

                          ),
                        )

                    ),
                    SizedBox(
                      width: 16.0,
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Flexible(
                fit: FlexFit.loose,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                        child: Text(widget.productname,
                          style: getStyleSubHeading(context)!.copyWith(
                              height: 1.5,
                              fontWeight: AppFont.fontWeightSemiBold),
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                        )),
                    Text(widget.modelnumber,
                      style: getStyleCaption(context)!.copyWith(height: 1.5),
                    ),

                    Text(widget.price,
                      style: getStyleBody2(context)!.copyWith(
                        color: colorPrimary,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Flexible(
                        child: widget.description == null ? Container():
                        Text("Description",
                          style: getStyleSubHeading(context)!.copyWith(
                              height: 1.5,
                              fontWeight: AppFont.fontWeightSemiBold),
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                        )),

                    Text(widget.description,
                      style: getStyleCaption(context)!.copyWith(height: 1.5),
                    ),

                    Container(
                      margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
                      width: double.infinity,
                      child:
                      RaisedButton(
                        color: colorAccent,
                        textColor: Colors.white,
                        elevation: 3.0,
                        padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                        child: Text("Back to Home",
                          style: getStyleButtonText(context),
                        ),
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0)),
                      ),
                    ),

                  ],
                ),
              ),
            )








          ],
        ),
      )


    );
  }

}
