import 'package:cart_app/core/helpers/app_colors.dart';
import 'package:cart_app/core/helpers/app_fonts.dart';
import 'package:cart_app/core/helpers/app_textstyle.dart';
import 'package:cart_app/core/helpers/app_validators.dart';
import 'package:cart_app/ui/home_page.dart';
import 'package:cart_app/ui/register_page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';



class LoginScreen extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // final log = getLogger('LoginScreen');
  TextEditingController _textEditConEmail =
  TextEditingController(); //raghunath@colanonline.com
  TextEditingController _textEditConPassword =
  TextEditingController(); //Pass@123
  var _keyValidationForm = GlobalKey<FormState>();
  final FocusNode _passwordFocus = FocusNode();
  bool _isPasswordVisible = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: colorGrey,
        body: SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(bottom: 32.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      _buildWidgetImageLogo(),
                      _buildWidgetLoginCard(context),
                    ],
                  )),
            ),

        );
  }


  Widget _buildWidgetImageLogo() {
    return Container(
      padding: EdgeInsets.only(top: 64, bottom: 32),
      alignment: Alignment.center,
      child: Image.network("https://www.freepnglogos.com/uploads/shopping-cart-png/shopping-cart-campus-recreation-university-nebraska-lincoln-30.png",
        width: 90,
        height: 120,
      ),
    );
  }

  Widget _buildWidgetLoginCard(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Card(
          color: colorWhite,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          elevation: 10.0,
          child: Padding(
            padding: const EdgeInsets.only(
                top: 24.0, bottom: 8.0, left: 24.0, right: 24.0),
            child: Form(
              key: _keyValidationForm,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: Text(
                      "LOGIN",
                      style: getFormTitleStyle(context),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: TextFormField(
                      controller: _textEditConEmail,
                      keyboardType: TextInputType.emailAddress,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.next,
                     validator: (value) {
                  if (value == null || value.isEmpty) {
                  return 'Please enter Username';
                  }
                  return null;
                  },
                      style: getFormStyleText(context),
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(_passwordFocus);
                      },
                      decoration: InputDecoration(
                          isDense: true,
                          labelText: "Username",
                          contentPadding: EdgeInsets.only(top: 4.0),
                          labelStyle: getFormLabelStyleText(context),


                      ),
                    ),
                  ), //text field: email
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: TextFormField(
                      controller: _textEditConPassword,
                      focusNode: _passwordFocus,
                      obscureText: !_isPasswordVisible,
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter Password';
                        }
                        return null;
                      },
                      style: getFormStyleText(context),
                      decoration: InputDecoration(
                        isDense: true,
                        //to reduce the size of icon, use if you want. I used, because my custom font icon is big
                        labelText: "Password",
                        contentPadding: EdgeInsets.only(top: 4.0),
                        //to make the base line of icon & text in same
                        labelStyle: getFormLabelStyleText(context),
                        suffixIcon: IconButton(
                          icon: Icon(_isPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off),
                          onPressed: () {
                            setState(() {
                              _isPasswordVisible = !_isPasswordVisible;
                            });
                          },
                        ),
                      ),
                    ),
                  ), //text field: password
                  Container(
                      margin: EdgeInsets.only(top: 16.0),
                      alignment: Alignment(1.0, 0.0),
                      child: InkWell(
                        splashColor: colorAccent.withOpacity(0.5),
                        onTap: () {

                        },
                        child: Text(
                          'Forgot Password?',
                          style: getStyleCaption(context)!.copyWith(
                              color: colorBlack,
                              fontWeight: AppFont.fontWeightSemiBold),
                        ),
                      )), //text: forgot password
                  Container(
                    margin: EdgeInsets.only(top: 20.0),
                    width: double.infinity,
                    child: RaisedButton(
                      color: colorAccent,
                      textColor: Colors.white,
                      elevation: 5.0,
                      padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                      child: Text(
                        "LOGIN",
                        style: getStyleButtonText(context),
                      ),
                      onPressed: () async {
                        FocusScope.of(context).requestFocus(FocusNode());

                        if (_keyValidationForm.currentState!.validate()) {
                          print('sucess');
                          _onClickButtonLogIn();
                        }else{
                          print("nah");
                        }


                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0)),
                    ),
                  ), //button: login
                  Container(
                      margin: EdgeInsets.only(top: 24.0, bottom: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'New to Cart',
                            style: getFormNormalTextStyle(context),
                          ),
                          InkWell(
                            splashColor: colorAccent.withOpacity(0.5),
                            onTap: () {
                              _onClickTextRegister(context);
                            },
                            child: Text(
                              ' Register',
                              style: getSmallTextNavigationStyle(context),
                            ),
                          )
                        ],
                      ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  void _onClickButtonLogIn() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var Userimage = "https://www.freeiconspng.com/thumbs/profile-icon-png/account-profile-user-icon--icon-search-engine-10.png";
   var username = prefs.getString('username');
  var password=  prefs.getString('password');
    if(username == _textEditConEmail.text && password ==  _textEditConPassword.text){

      Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(username.toString(),Userimage)));

    }
else{
      _showSnackBarMessage("Invalid credentials");
    }
      }


  void _onClickTextRegister(BuildContext context) {

    Navigator.push(context, MaterialPageRoute(builder: (context) => RegisterScreen()));

  }




  //show: snackBar toast
  void _showSnackBarMessage(String message) {
    final snackBar = SnackBar(content: Text(message));
    _scaffoldKey.currentState!.showSnackBar(snackBar);
  }


}
