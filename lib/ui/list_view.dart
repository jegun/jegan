import 'package:cart_app/core/providers/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class ItemView extends StatefulWidget {
  final int _activeTabIndex;
ItemView(this._activeTabIndex);
  @override
  _ItemViewState createState() => _ItemViewState();
}

class _ItemViewState extends State<ItemView> {
  TextEditingController _textEditingController = TextEditingController();
  TextEditingController _textEditingController1 = TextEditingController();
  TextEditingController _textEditingController2 = TextEditingController();
  TextEditingController _textEditingController4 = TextEditingController();

  static var _keyValidationForm = GlobalKey<FormState>();



  @override
  void initState() {
    print(widget._activeTabIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Add item'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key:_keyValidationForm ,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextFormField(
                      controller: _textEditingController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter Product name';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText:" Product name"
                      ),
                      onFieldSubmitted: (value) => submit(),
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: true,
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _textEditingController1,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter Model number';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: 'Model number'
                      ),
                      onFieldSubmitted: (value) => submit(),
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: true,
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _textEditingController2,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter price';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: 'Price'
                      ),
                      onFieldSubmitted: (value) => submit(),
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: true,
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      controller: _textEditingController4,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter description';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          labelText: 'Description'
                      ),
                      onFieldSubmitted: (value) => submit(),
                      textCapitalization: TextCapitalization.sentences,
                      autofocus: true,
                    ),
                    SizedBox(height: 10),

                    RaisedButton(
                      child: Text('Submit'),
                      onPressed: submit,


                    )
                  ],
                ),
              )

          ),
        )

    );
  }

  void submit() async{

    String Productname = _textEditingController.text;
    String modelnumber = _textEditingController1.text;
    String price = _textEditingController2.text;
    String Description = _textEditingController2.text;
    int tabindex =widget._activeTabIndex;

    context.read<StateProvider>().addNewTask(Productname,modelnumber,price,Description,tabindex);

      Navigator.pop(context, _textEditingController.text);
    }

}